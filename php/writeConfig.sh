#!/bin/bash

cat > config.php <<_END_
<?php

$host       = "$DBHOST";
$username   = "$DBUSER";
$password   = "$DBPW";
$dbname     = "$DBNAME";
$dsn        = "mysql:host=$host;dbname=$dbname";
$options    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );
_END_
