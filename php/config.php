<?php

$host       = getenv("DBHOST");
$username   = getenv("DBUSER");
$password   = getenv("DBPW");
$dbname     = getenv("DBNAME");
$dsn        = "mysql:host=$host;dbname=$dbname";
$options    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );
