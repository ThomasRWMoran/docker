#!/bin/bash
docker build -t tomdb:1.0 -f Dockerfile.DB .
docker run -itd --name TomDB -P -e MYSQL_ROOT_PASSWORD=secret123 tomdb:1.0
sleep 10
dbport=$(docker port TomDB | grep '^3306' | awk -F: '{print $NF}')
dbresult=$(mysql -h 127.0.0.1 -P $dbport -u root -psecret123 -D petclinic --execute="select * from owners;")
if [ -z "$dbresult" ]; then
    echo "Will delete DB"
    docker rm -f TomDB
    docker rmi tomdb:1.0
    exit 1
fi
echo "DB will stay up"

docker build -t tompc:1.0 -f Dockerfile.PC .
if ! docker images | grep tompc; then
  echo "PC imaged didn't build. Burn it all"
  docker rm -f TomDB
  docker rmi tomdb:1.0
  exit 2
fi
docker run -itd --name TomPC -P --link="TomDB:pcdb" -e DBHOST=pcdb -e DBUSER=root -e DBPW=secret123 tompc:1.0

# curl 127.0.0.1:$(docker port TomPC | grep '^8080' | awk -F: '{print $NF}')
